﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using OTDAV.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Controllers
{
    public class UserConstituentFileController : Controller
    {


        // GET: UserConstituentFile
        public async Task<ActionResult> Index(int? id)
        {
            ViewBag.idMembership = id;
            if (id.HasValue)
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE + "/bymembership/" + id);
                    HttpResponseMessage response = await client.GetAsync(JavaSpring.CONSTITUENT_FILE + "/bymembership/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        ICollection<ConstituentFile> list = JsonConvert.DeserializeObject<ICollection<ConstituentFile>>(rawData);
                        return View(list);
                    }
                    else
                    {
                        return View(new List<ConstituentFile>());
                    }
                }
                
            } else
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE, User.Identity.Name);
                    HttpResponseMessage response = await client.GetAsync(JavaSpring.CONSTITUENT_FILE);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        ICollection<ConstituentFile> list = JsonConvert.DeserializeObject<ICollection<ConstituentFile>>(rawData);
                        return View(list);
                    }
                    else
                    {
                        return View();
                    }
                }
            }
        }

        // GET: UserConstituentFile/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.CONSTITUENT_FILE);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    ICollection<ConstituentFile> fileList = JsonConvert.DeserializeObject<ICollection<ConstituentFile>>(rawData);
                    ConstituentFile file = fileList.FirstOrDefault(f => f.id == id);
                    if(file != null)
                    {
                        return this.GetFile(file.path);
                    } else
                    {
                        return new HttpNotFoundResult();
                    }
                   
                }
                else
                {
                    return new HttpNotFoundResult();
                }
            }
        }

        // GET: UserConstituentFile/Create
        public ActionResult Create(int? id)
        {
            AppFile appFile = new AppFile();
            if(id.HasValue)
            {
                appFile.idMembership = id.Value;
            }
            return View(appFile);
        }

        // POST: UserConstituentFile/Create
        [HttpPost]
        public async Task<ActionResult> Create(AppFile formCollection)
        {
            try
            {
                // Initialization.  
                string filePath = string.Empty;
                string fileContentType = string.Empty;
                if (ModelState.IsValid)
                {
                    // Converting to bytes.  
                    byte[] uploadedFile = new byte[formCollection.FileAttach.InputStream.Length];
                    formCollection.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                    // Initialization.  
                    fileContentType = formCollection.FileAttach.ContentType;
                    string folderPath = "~/Content/upload_files/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, formCollection.FileAttach.FileName);
                    filePath = folderPath + formCollection.FileAttach.FileName;

                    using (HttpClient client = new HttpClient())
                    {
                        ConstituentFile cFile = new ConstituentFile();
                        cFile.path = filePath;
                        //this function add headers and other things like request return format ( json )
                        HttpResponseMessage response;
                        if (formCollection.idMembership != 0) {
                            cFile.membership = new Areas.Admin.Models.Membership();
                            cFile.membership.id = formCollection.idMembership;

                        }
                        client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE);
                        response = await client.PostAsJsonAsync(JavaSpring.CONSTITUENT_FILE, cFile);

                        if (response.IsSuccessStatusCode)
                        {
                            var rawData = await response.Content.ReadAsStringAsync();
                            return RedirectToAction("Index", new { id = cFile.membership.id });
                        }
                        else
                        {
                            return View();
                        }
                    }
                    //this.databaseManager.sp_insert_file(model.FileAttach.FileName, fileContentType, filePath);
                }
                    // TODO: Add insert logic here

                    return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //public ActionResult DownloadFile(int fileId)
        //{
        //    // Model binding.  
        //    ImgViewModel model = new ImgViewModel { FileAttach = null, ImgLst = new List<ImgObj>() };

        //    try
        //    {
        //        // Loading dile info.  
        //        var fileInfo = this.databaseManager.sp_get_file_details(fileId).First();

        //        // Info.  
        //        return this.GetFile(fileInfo.file_path);
        //    }
        //    catch (Exception ex)
        //    {
        //        // Info  
        //        Console.Write(ex);
        //    }

        //    // Info.  
        //    return this.View(model);
        //}

        #region Write to file  

        /// <summary>  
        /// Write content to file.  
        /// </summary>  
        /// <param name="rootFolderPath">Root folder path parameter</param>  
        /// <param name="fileBytes">File bytes parameter</param>  
        /// <param name="filename">File name parameter</param>  
        private void WriteBytesToFile(string rootFolderPath, byte[] fileBytes, string filename)
        {
            try
            {
                // Verification.  
                if (!Directory.Exists(rootFolderPath))
                {
                    // Initialization.  
                    string fullFolderPath = rootFolderPath;

                    // Settings.  
                    string folderPath = new Uri(fullFolderPath).LocalPath;

                    // Create.  
                    Directory.CreateDirectory(folderPath);
                }

                // Initialization.                  
                string fullFilePath = rootFolderPath + filename;

                // Create.  
                FileStream fs = System.IO.File.Create(fullFilePath);

                // Close.  
                fs.Flush();
                fs.Dispose();
                fs.Close();

                // Write Stream.  
                BinaryWriter sw = new BinaryWriter(new FileStream(fullFilePath, FileMode.Create, FileAccess.Write));

                // Write to file.  
                sw.Write(fileBytes);

                // Closing.  
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch (Exception ex)
            {
                // Info.  
                throw ex;
            }
        }

        #endregion

        #region Get file method.  

        /// <summary>  
        /// Get file method.  
        /// </summary>  
        /// <param name="filePath">File path parameter.</param>  
        /// <returns>Returns - File.</returns>  
        private FileResult GetFile(string filePath)
        {
            // Initialization.  
            FileResult file = null;

            try
            {
                // Initialization.  
                string contentType = MimeMapping.GetMimeMapping(filePath);

                // Get file.  
                file = this.File(filePath, contentType);
            }
            catch (Exception ex)
            {
                // Info.  
                throw ex;
            }

            // info.  
            return file;
        }

        #endregion

        // GET: UserConstituentFile/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.CONSTITUENT_FILE + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    ConstituentFile file = JsonConvert.DeserializeObject<ConstituentFile>(rawData);
                    return View(file);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: UserConstituentFile/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, ConstituentFile collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.CONSTITUENT_FILE + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.CONSTITUENT_FILE + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
