﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Controllers
{
    public class UserDistrubtionRightController : Controller
    {
        // GET: DistrubtionRight
        public ActionResult Index()
        {
            return View();
        }

        // GET: DistrubtionRight/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: DistrubtionRight/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DistrubtionRight/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DistrubtionRight/Edit/5
        public async Task<ActionResult> Edit(int  idDistrubition, int IdWork, bool backtowork=false)
        {
            ViewBag.backtowork = backtowork;
            ICollection<Membership> CurMembership = null;
            try
            {
                HttpClient MemberShipClient = new HttpClient();
                //MemberShip
                {
                    MemberShipClient.PrepareForRequest(JavaSpring.MEMBERSHIPS, User.Identity.Name);
                    HttpResponseMessage MemberShipResponse = await MemberShipClient.GetAsync(JavaSpring.MEMBERSHIPS);
                    if (MemberShipResponse.IsSuccessStatusCode)
                    {
                        string rawData = await MemberShipResponse.Content.ReadAsStringAsync();
                        CurMembership = JsonConvert.DeserializeObject<ICollection<Membership>>(rawData);
                        MemberShipClient.Dispose();
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                throw;
            }
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + idDistrubition);
                    HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + idDistrubition);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        OTDAV.Areas.Admin.Models.DistributionRight distributionRight = JsonConvert.DeserializeObject<OTDAV.Areas.Admin.Models.DistributionRight>(rawData);
                        distributionRight.membership = new OTDAV.Areas.Admin.Models.Membership();
                        distributionRight.membership.id = CurMembership.ElementAt(0).id;
                        distributionRight.work = new OTDAV.Areas.Admin.Models.Work();
                        distributionRight.work.id = IdWork;
                        return View(distributionRight);
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: DistrubtionRight/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, OTDAV.Areas.Admin.Models.DistributionRight CurDistrubtionRight, bool backtowork = false)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id, CurDistrubtionRight);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        if(backtowork)
                        {
                            return RedirectToAction("Details", "Work", new { CurDistrubtionRight.work.id});
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: DistrubtionRight/Delete/5
        public async Task<ActionResult> Delete(int id, int IdWork, bool backtowork = false)
        {
            ViewBag.backtowork = backtowork;
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    OTDAV.Areas.Admin.Models.DistributionRight CurDistrubtionRight = JsonConvert.DeserializeObject<OTDAV.Areas.Admin.Models.DistributionRight>(rawData);
                    CurDistrubtionRight.work = new OTDAV.Areas.Admin.Models.Work();
                    CurDistrubtionRight.work.id = IdWork;
                    return View(CurDistrubtionRight);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: DistrubtionRight/Delete/5
        [HttpPost]
        public async Task<ActionResult>  Delete(int id, OTDAV.Areas.Admin.Models.DistributionRight CurDistrubtionRight, bool backtowork = false)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + CurDistrubtionRight.id);
                HttpResponseMessage response = await client.DeleteAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + CurDistrubtionRight.id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    if (backtowork)
                    {
                        return RedirectToAction("Details", "Work", new { CurDistrubtionRight.work.id });
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
        }
    }
}
