﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using OTDAV.Core;
using OTDAV.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Controllers
{
    public class UserMembershipController : Controller
    {

        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;


        public UserMembershipController()
        {

        }

        public UserMembershipController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;


        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        // GET: Membership
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.MEMBERSHIPS, User.Identity.Name);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.MEMBERSHIPS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Areas.Admin.Models.Membership> list = JsonConvert.DeserializeObject<ICollection<Areas.Admin.Models.Membership>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Areas.Admin.Models.Membership>());
                }
            }
        }

        // GET: Membership/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult CreateMembershipOnly()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateMembershipOnly(Membership collection)
        {
            // creation first time membership
            using (HttpClient clientMembership = new HttpClient())
            {
                //collection.user = null;
                clientMembership.PrepareForRequest(JavaSpring.MEMBERSHIPS, User.Identity.Name);
                HttpResponseMessage membershipResponse = await clientMembership.PostAsJsonAsync(JavaSpring.MEMBERSHIPS, collection);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: Membership/Create
        public async Task<ActionResult> Create()
        {
            using (HttpClient client = new HttpClient())
            {
                client.PrepareForRequest(JavaSpring.GUESTS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.GUESTS);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    ICollection<OTDAV.Areas.Admin.Models.User> list = JsonConvert.DeserializeObject<ICollection<OTDAV.Areas.Admin.Models.User>>(rawData);
                    if(list.Count == 0)
                    {
                        return RedirectToAction("CreateMembershipOnly");
                    }
                    Membership m  = new Membership();
                    m.stat = "In_progress";
                    m.user = list.Where(u=> u.email == User.Identity.Name).FirstOrDefault();
                    m.user.moral = true;
                    m.user.isSubscribed = true;
                    return View(m);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Membership/Create
        [HttpPost]
        public async Task<ActionResult> Create(Membership collection)
        {
            try
            {


                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.USERS +"/save");
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.USERS + "/save", collection.user);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        var currentUser = await UserManager.FindByEmailAsync(User.Identity.Name);
                        await UserManager.RemoveFromRoleAsync(currentUser.Id, "guest");
                        var roleresult = await UserManager.AddToRoleAsync(currentUser.Id, "user");
                        // creation first time membership
                        using (HttpClient clientMembership = new HttpClient())
                        {
                            collection.user = null;
                            clientMembership.PrepareForRequest(JavaSpring.MEMBERSHIPS, User.Identity.Name);
                            HttpResponseMessage membershipResponse = await clientMembership.PostAsJsonAsync(JavaSpring.MEMBERSHIPS, collection);
                            await SignInManager.SignInAsync(currentUser, isPersistent: false, rememberBrowser: false);
                        }
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View(collection);
                    }

                }

            }
            catch
            {
                return View();
            }
        }

        // GET: Membership/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Membership/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Membership/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Membership/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
