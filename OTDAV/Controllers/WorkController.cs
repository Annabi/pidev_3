﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OTDAV.Controllers
{
    public class WorkController : Controller
    {
        // GET: Work
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORK, User.Identity.Name);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORK);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<OTDAV.Areas.Admin.Models.Work> list = JsonConvert.DeserializeObject<ICollection<OTDAV.Areas.Admin.Models.Work>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<OTDAV.Areas.Admin.Models.Work>());
                }
            }
        }

        // GET: Work/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORK + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORK + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Work work = JsonConvert.DeserializeObject<Work>(rawData);
                    return View(work);
                }
                else
                {
                    return View();
                }
            }
        }

        public async Task<ICollection<WorkCategory>> RetriveWorkCategory()
        {
            List<WorkCategory> WorkCategoryList = new List<WorkCategory>();
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORKCATEGORY);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORKCATEGORY);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<WorkCategory> list = JsonConvert.DeserializeObject<ICollection<WorkCategory>>(rawData);
                    return list;
                }
            }
            return null;
        }
        // GET: Work/Create
        public ActionResult Create()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    ICollection<WorkCategory> result = Task.Run(RetriveWorkCategory).Result;
                    List<SelectListItem> ListWorkCategorie = result.Select(workCat =>
                   {
                       return new SelectListItem() { Text = workCat.title, Value = workCat.id.ToString() };
                   }).ToList();
                    ViewBag.ListWorkCategorie = ListWorkCategorie;
                    return View();
                }
            }
            catch
            {
                return View();
            }

        }

        // POST: Admin/WorkCategory/Create
        [HttpPost]
        public async Task<ActionResult> Create(Work CurWork)
        {
            Notification NewNotif = new Notification();
            ICollection<Membership> CurMembership = null;

            try
            {
                HttpClient NotificationClient = new HttpClient();
                NewNotif.stat = "In_progress";
                NewNotif.date = DateTime.Now;
                NotificationClient.PrepareForRequest(JavaSpring.NOTIFICATION);
                HttpResponseMessage NotificationResponse = await NotificationClient.PostAsJsonAsync(JavaSpring.NOTIFICATION, NewNotif);
                if (NotificationResponse.IsSuccessStatusCode)
                {
                    var rawData = await NotificationResponse.Content.ReadAsStringAsync();
                    int test = rawData.LastIndexOf('=') + 1;
                    int test1 = rawData.Length - 1;
                    string id = rawData.Substring(test);
                    NewNotif.id = Convert.ToInt32(id);
                    CurWork.notification = NewNotif;
                }
            }
            catch (HttpRequestException ex)
            {
                return View();
            }

            try
            {
                HttpClient MemberShipClient = new HttpClient();
                //MemberShip
                {
                    MemberShipClient.PrepareForRequest(JavaSpring.MEMBERSHIPS, User.Identity.Name);
                    HttpResponseMessage MemberShipResponse = await MemberShipClient.GetAsync(JavaSpring.MEMBERSHIPS);
                    if (MemberShipResponse.IsSuccessStatusCode)
                    {
                        string rawData = await MemberShipResponse.Content.ReadAsStringAsync();
                        CurMembership = JsonConvert.DeserializeObject<ICollection<Membership>>(rawData);
                        CurWork.membership = CurMembership.ElementAt(0);
                        MemberShipClient.Dispose();
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                return View();
            }

            try
            {
                if (CurMembership != null)
                {
                    HttpClient WorkClient = new HttpClient();
                    WorkClient.PrepareForRequest(JavaSpring.WORK, User.Identity.Name);
                    HttpResponseMessage WorkCreaionResponse = await WorkClient.PostAsJsonAsync(JavaSpring.WORK, CurWork);
                    if (WorkCreaionResponse.IsSuccessStatusCode)
                    {
                        var rawData = await WorkCreaionResponse.Content.ReadAsStringAsync();
                        int test = rawData.LastIndexOf('=') + 1;
                        int test1 = rawData.Length - 1;
                        string id = rawData.Substring(test);
                        CurWork.id = Convert.ToInt32(id);

                        WorkClient.Dispose();
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                return View();
            }

            if (CurWork.distributionRights != null)
            {
                foreach (OTDAV.Areas.Admin.Models.DistributionRight item in CurWork.distributionRights)
                {
                    try
                    {
                        OTDAV.Models.DistributionRight TempDistrubtionRight = new OTDAV.Models.DistributionRight();

                        TempDistrubtionRight.work = new OTDAV.Models.Work();
                        TempDistrubtionRight.membership = new OTDAV.Models.Membership();

                        TempDistrubtionRight.id = item.id;
                        TempDistrubtionRight.price = item.price;
                        TempDistrubtionRight.work.id = CurWork.id;
                        TempDistrubtionRight.description = item.description;
                        TempDistrubtionRight.startingDate = item.startingDate;
                        TempDistrubtionRight.finishingDate = item.finishingDate;
                        TempDistrubtionRight.membership.id = CurMembership.ElementAt(0).id;

                        HttpClient DistrubtionRightclient = new HttpClient();
                        DistrubtionRightclient.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT);
                        HttpResponseMessage DistrubtionRightResponse = await DistrubtionRightclient.PostAsJsonAsync(JavaSpring.DISTRUBTIONRIGHT, TempDistrubtionRight);

                        if (DistrubtionRightResponse.IsSuccessStatusCode)
                        {
                            DistrubtionRightclient.Dispose();
                        }
                    }
                    catch (HttpRequestException ex)
                    {
                        return View();
                    }
                }
            }
            return RedirectToAction("Index");
        }


        // GET: Work/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORK + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORK + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Work work = JsonConvert.DeserializeObject<Work>(rawData);
                    return View(work);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Work/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Work collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.WORK + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.WORK + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }


        // GET: Work/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORK + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORK + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    OTDAV.Areas.Admin.Models.Work work = JsonConvert.DeserializeObject<OTDAV.Areas.Admin.Models.Work>(rawData);
                    return View(work);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Work/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, OTDAV.Areas.Admin.Models.Work collection)
        {
            int NotificationIdToDelete = -1;
            List<DistributionRight> DistributionRightsToDelete = null;
            try
            {
                HttpClient RetriveDataClient = new HttpClient();
                HttpResponseMessage RetriveDataresponse = await RetriveDataClient.GetAsync(JavaSpring.WORK + "/" + id);
                if (RetriveDataresponse.IsSuccessStatusCode)
                {
                    string rawData = await RetriveDataresponse.Content.ReadAsStringAsync();
                    OTDAV.Areas.Admin.Models.Work WorkList = JsonConvert.DeserializeObject<OTDAV.Areas.Admin.Models.Work>(rawData);
                    if(WorkList.notification != null)
                        NotificationIdToDelete = WorkList.notification.id;
                    if (WorkList.distributionRights != null)
                        DistributionRightsToDelete = WorkList.distributionRights;
                }
            }
            catch (Exception)
            {

                throw;
            }

            try
            {
                HttpClient NotificationClient = new HttpClient();
                NotificationClient.PrepareForRequest(JavaSpring.NOTIFICATION + "/" + id);
                HttpResponseMessage NotificationResponse = await NotificationClient.DeleteAsync(JavaSpring.NOTIFICATION + "/" + NotificationIdToDelete);
                if (NotificationResponse.IsSuccessStatusCode)
                {
                    var rawData = await NotificationResponse.Content.ReadAsStringAsync();
                }
            }
            catch (Exception)
            {

                throw;
            }

            if (DistributionRightsToDelete != null)
            {
                foreach (DistributionRight CurDistrubtionRight in DistributionRightsToDelete)
                {
                    using (HttpClient client = new HttpClient())
                    {
                        //this function add headers and other things like request return format ( json )
                        client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + CurDistrubtionRight.id);
                        HttpResponseMessage response = await client.DeleteAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + CurDistrubtionRight.id);
                        if (response.IsSuccessStatusCode)
                        {
                            var rawData = await response.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            return View();
                        }

                    }
                }
            }

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.WORK + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.WORK + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}

