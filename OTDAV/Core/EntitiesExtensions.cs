﻿using Microsoft.AspNet.Identity.EntityFramework;
using OTDAV.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Core
{
    public static class EntitiesExtensions
    {
        public static void UpdateRole(this IdentityRole iRole, Role role)
        {
            iRole.Id = role.Id;
            iRole.Name = role.Name;
            //iRole.Users = role.Users;
        }
        public static void mapToRole(this Role iRole, IdentityRole role)
        {
            iRole.Id = role.Id;
            iRole.Name = role.Name;
            //iRole.Users = role.Users;
        }
    }
}