﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace OTDAV.Core
{
    public class JavaSpring
    {

        //public const string EXAMPLE_API_PREFIX = "http://dummy.restapiexample.com/api/v1/";

        public const string API_PREFIX = "http://localhost:8080/";

        public static string NOTIFICATION { get { return API_PREFIX + "notification"; } }
        public static string MEMBERSHIPS { get { return API_PREFIX + "memberships"; } }
        
        public static string WORKCATEGORY { get { return API_PREFIX + "workcategorie"; } }

        public static string WORK { get { return API_PREFIX + "work"; } }

        public static string DISTRUBTIONRIGHT { get { return API_PREFIX + "DistrubtionRight"; } }
        public static string PERSON { get { return API_PREFIX + "person"; } }

        public static string CONSTITUENT_FILE { get { return API_PREFIX + "constituentFile"; } }
        public static string EMPLOYEES { get { return API_PREFIX + "employees"; } }
        public static string ROLES { get { return API_PREFIX + "role"; } }
        public static string GUESTS { get { return API_PREFIX + "guest"; } }
        public static string ADMINISTRATORS { get { return API_PREFIX + "administrator"; } }
        public static string AGENTS { get { return API_PREFIX + "agent"; } }
        public static string USERS { get { return API_PREFIX + "user"; } }


        public static string EMPLOYEE { get { return API_PREFIX + "employee/"; } }
        //****AA******
        public static string DEPARTMENTS { get { return API_PREFIX + "departement";} }
        public static string AGENTS_OF_DEPARTMENT { get { return "agents";} }

        //******AA*****


    }
}