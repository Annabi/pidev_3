﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace OTDAV.Core
{
    public static class ExtensionMethods
    {
        public static HttpClient PrepareForRequest(this HttpClient client, string url, string email = null)
        {
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new
            System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            if(email != null)
            {
                client.DefaultRequestHeaders.Add("email", email);

            };
            return client;
        }
    }
}