﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OTDAV.Startup))]
namespace OTDAV
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
