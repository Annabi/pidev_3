﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;

namespace OTDAV.Areas.Admin.Controllers

{
    [Authorize(Roles = "admin")]
    public class DepartmentController : Controller
    {


        //GET:Admin/Department/5/Agents

        public async Task<ActionResult> Agents(int id)

        {
            System.Diagnostics.Debug.WriteLine("This will be displayed in output window from head");

            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id + "/" + JavaSpring.AGENTS_OF_DEPARTMENT);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS + "/" + id + "/" + JavaSpring.AGENTS_OF_DEPARTMENT);

                System.Diagnostics.Debug.WriteLine("This will be displayed in output window from head2");
                System.Diagnostics.Debug.WriteLine(response.IsSuccessStatusCode);

                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Agent> list = JsonConvert.DeserializeObject<ICollection<Agent>>(rawData);
                    return View(list);

                    Console.WriteLine(response);
                    System.Diagnostics.Debug.WriteLine("This will be displayed in output window from if");

                    Console.WriteLine(list);
                }
                else
                {
                    return View(new List<Agent>());
                    Console.WriteLine(response);
                    System.Diagnostics.Debug.WriteLine("This will be displayed in output window from else");
                }
            }
        }


        //GET:Admin/Department

        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DEPARTMENTS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Department> list = JsonConvert.DeserializeObject<ICollection<Department>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Department>());
                }
            }
        }

        //GET Admin/Department/Details/5

        public async Task<ActionResult> Details(int id)
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id);
                    HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        Department department = JsonConvert.DeserializeObject<Department>(rawData);
                        return View(department);
                    }
                    else
                    {
                        return View();
                    }
                }
            }


        // GET: Admin/Department/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: Admin/Department/Create
        [HttpPost]
        public async Task<ActionResult> Create(Department collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DEPARTMENTS);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.DEPARTMENTS, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Department/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Department department = JsonConvert.DeserializeObject<Department>(rawData);
                    return View(department);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Membership/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Department collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.DEPARTMENTS + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Department/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
                            using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id);
                    HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        Department department = JsonConvert.DeserializeObject<Department>(rawData);
                        return View(department);
                    }
                    else
                    {
                        return View();
                    }
                }
        }

        // POST: Admin/Department/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(string id, Department collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.DEPARTMENTS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }


    }
}