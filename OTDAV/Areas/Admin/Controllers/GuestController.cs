﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;

namespace OTDAV.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class GuestController : Controller
    {

        public async Task<ActionResult> ConvertAgent(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.GUESTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.GUESTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);

                    using (HttpClient clt = new HttpClient())
                    {
                        //this function add headers and other things like request return format ( json )
                        clt.PrepareForRequest(JavaSpring.PERSON + "/agent");
                        HttpResponseMessage resp = await clt.PostAsJsonAsync(JavaSpring.PERSON + "/agent", person);
                        System.Diagnostics.Debug.WriteLine(resp.IsSuccessStatusCode);
                        if (resp.IsSuccessStatusCode)
                        {
                            var rawData2 = await resp.Content.ReadAsStringAsync();
                            //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);

                            return RedirectToAction("../Agent/Index");
                        }



                    }
                }



            }






            return RedirectToAction("Index");
        }


        // GET: Admin/Guest
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.GUESTS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.GUESTS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Guest> list = JsonConvert.DeserializeObject<ICollection<Guest>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Guest>());
                }
            }
        }

        // GET: Admin/Guest/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.GUESTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.GUESTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Guest person = JsonConvert.DeserializeObject<Guest>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }



        // GET: Admin/Guest/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.GUESTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.GUESTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Guest person = JsonConvert.DeserializeObject<Guest>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Guest/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Guest collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.GUESTS + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.GUESTS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
