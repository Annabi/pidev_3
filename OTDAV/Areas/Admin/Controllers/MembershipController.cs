﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class MembershipController : Controller
    {
        // GET: Admin/Membership
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.MEMBERSHIPS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.MEMBERSHIPS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Membership> list = JsonConvert.DeserializeObject<ICollection<Membership>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Membership>());
                }
            }
        }

        // GET: Admin/Membership/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.MEMBERSHIPS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.MEMBERSHIPS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                    return View(membership);
                }
                else
                {
                    return View();
                }
            }
        }

        // GET: Admin/Membership/Create
        public async Task<ActionResult> Create()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                HttpResponseMessage response;
                client.PrepareForRequest(JavaSpring.USERS + "/all");
                response = await client.GetAsync(JavaSpring.USERS + "/all");
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Person> list = JsonConvert.DeserializeObject<ICollection<Person>>(rawData);
                    List<SelectListItem> listUser = list.Select(user =>
                    {
                        return new SelectListItem() { Text = user.email, Value = user.id.ToString() };
                    }).ToList();
                    ViewBag.listUser = listUser;
                    return View();
                }
                return View();
            }
        }


        // POST: Admin/Membership/Create
        [HttpPost]
        public async Task<ActionResult> Create(Membership collection)
        {
            try
            {
                List<SelectListItem> listUser;
                ICollection<Person> list = new List<Person>();
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    HttpResponseMessage response;
                    client.PrepareForRequest(JavaSpring.USERS + "/all");
                    response = await client.GetAsync(JavaSpring.USERS + "/all");
                    if (response.IsSuccessStatusCode)
                    {
                        string rawData = await response.Content.ReadAsStringAsync();
                        list = JsonConvert.DeserializeObject<ICollection<Person>>(rawData);
                        listUser = list.Select(user =>
                        {
                            return new SelectListItem() { Text = user.email, Value = user.id.ToString() };
                        }).ToList();
                        ViewBag.listUser = listUser;
                    }
                }
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    if(collection.user != null && list.Any(u=> u.id == collection.user.id))
                    {
                        client.PrepareForRequest(JavaSpring.MEMBERSHIPS, list.FirstOrDefault(u => u.id == collection.user.id).email);
                    } else
                    {
                        client.PrepareForRequest(JavaSpring.MEMBERSHIPS);
                    }
                    
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.MEMBERSHIPS, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Membership/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.MEMBERSHIPS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.MEMBERSHIPS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                    return View(membership);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Membership/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Membership collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.MEMBERSHIPS + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.MEMBERSHIPS + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Membership/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/Membership/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.MEMBERSHIPS + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.MEMBERSHIPS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
