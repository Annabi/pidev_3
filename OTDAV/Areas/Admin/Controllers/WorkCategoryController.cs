﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Areas.Admin.Controllers
{
    public class WorkCategoryController : Controller
    {
        // GET: Admin/WorkCategory
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORKCATEGORY);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORKCATEGORY);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<WorkCategory> list = JsonConvert.DeserializeObject<ICollection<WorkCategory>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<WorkCategory>());
                }
            }
        }

        // GET: Admin/WorkCategory/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORKCATEGORY + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORKCATEGORY + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    WorkCategory workcategory = JsonConvert.DeserializeObject<WorkCategory>(rawData);
                    return View(workcategory);
                }
                else
                {
                    return View();
                }
            }
        }

        // GET: Admin/WorkCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/WorkCategory/Create
        [HttpPost]
        public async Task<ActionResult> Create(WorkCategory collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.WORKCATEGORY);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.WORKCATEGORY, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/WorkCategory/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORKCATEGORY + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORKCATEGORY + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    WorkCategory workcategory = JsonConvert.DeserializeObject<WorkCategory>(rawData);
                    return View(workcategory);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/WorkCategory/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, WorkCategory collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.WORKCATEGORY + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.WORKCATEGORY + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/WorkCategory/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.WORKCATEGORY + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.WORKCATEGORY + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    WorkCategory workcategory = JsonConvert.DeserializeObject<WorkCategory>(rawData);
                    return View(workcategory);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/WorkCategory/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, WorkCategory collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.WORKCATEGORY + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.WORKCATEGORY + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
