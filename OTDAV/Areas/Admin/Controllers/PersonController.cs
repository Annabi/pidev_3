﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;

namespace OTDAV.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]

    public class PersonController : Controller
    {


        public async Task<ActionResult> ConvertAgent(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);

                    using (HttpClient clt = new HttpClient())
                    {
                        //this function add headers and other things like request return format ( json )
                        clt.PrepareForRequest(JavaSpring.PERSON + "/agent");
                        HttpResponseMessage resp = await clt.PostAsJsonAsync(JavaSpring.PERSON + "/agent", person);
                        System.Diagnostics.Debug.WriteLine(resp.IsSuccessStatusCode);
                        if (resp.IsSuccessStatusCode)
                        {
                            var rawData2 = await resp.Content.ReadAsStringAsync();
                            //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);

                            return RedirectToAction("Index");
                        }



                    }
                }



            }






            return RedirectToAction("Index");
        }

        // GET: Admin/Person
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Person> list = JsonConvert.DeserializeObject<ICollection<Person>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Person>());
                }
            }
        }

        // GET: Admin/Person/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }

        // GET: Admin/Person/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Person/Create
        [HttpPost]
        public async Task<ActionResult> Create(Person collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.PERSON);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.PERSON, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Person/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Person/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Person collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.PERSON + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Person/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Person/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Person collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.PERSON + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
