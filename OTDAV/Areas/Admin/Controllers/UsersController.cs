﻿using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Areas.Admin.Controllers
{

    public class UsersController : Controller
    {
        private ApplicationUserManager _userManager;

        public UsersController()
        {

        }

        public UsersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult List(FormCollection role)
        {
            return RedirectToAction("Index", new { role = role["role"] });

        }
        // GET: Admin/User
        public async Task<ActionResult> Index(string role)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                HttpResponseMessage response;
                switch (role)
                {
                    case "admin":
                        client.PrepareForRequest(JavaSpring.ADMINISTRATORS);
                        response = await client.GetAsync(JavaSpring.ADMINISTRATORS);
                        break;
                    case "agent":
                        client.PrepareForRequest(JavaSpring.AGENTS);
                        response = await client.GetAsync(JavaSpring.AGENTS);
                        break;
                    case "user":
                        client.PrepareForRequest(JavaSpring.USERS + "/all");
                        response = await client.GetAsync(JavaSpring.USERS + "/all");
                        break;
                    default:
                        client.PrepareForRequest(JavaSpring.GUESTS);
                        response = await client.GetAsync(JavaSpring.GUESTS);
                        break;
                }

                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Person> list = JsonConvert.DeserializeObject<ICollection<Person>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<Person>());
                }
            }
        }

        // GET: Admin/User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/User/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Person/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Person person = JsonConvert.DeserializeObject<Person>(rawData);
                    return View(person);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Person/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Person collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {

                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.PERSON + "/" + id);
                    HttpResponseMessage userResp = await client.GetAsync(JavaSpring.PERSON + "/" + id);
                    if (userResp.IsSuccessStatusCode)
                    {
                        var rawData = await userResp.Content.ReadAsStringAsync();
                        Person person = JsonConvert.DeserializeObject<Person>(rawData);
                        HttpResponseMessage response = await client.DeleteAsync(JavaSpring.PERSON + "/" + id);
                        if (response.IsSuccessStatusCode)
                        {
                            var user = await UserManager.FindByEmailAsync(person.email);
                            await UserManager.DeleteAsync(user);
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            return View();
                        }

                    } else
                    {
                        return View();
                    }


                }
            }
            catch
            {
                return View();
            }
        }
    }
}
