﻿using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OTDAV.Areas.Admin.Controllers
{
    public class DistrubtionRightController : Controller
    {
        // GET: Admin/DistrubtionRight
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<DistributionRight> list = JsonConvert.DeserializeObject<ICollection<DistributionRight>>(rawData);
                    return View(list);
                }
                else
                {
                    return View(new List<DistributionRight>());
                }
            }
        }

        // GET: Admin/DistrubtionRight/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    DistributionRight distrubtionright = JsonConvert.DeserializeObject<DistributionRight>(rawData);
                    return View(distrubtionright);
                }
                else
                {
                    return View();
                }
            }
        }

        // GET: Admin/DistrubtionRight/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/DistrubtionRight/Create
        [HttpPost]
        public async Task<ActionResult> Create(DistributionRight collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.DISTRUBTIONRIGHT, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/DistrubtionRight/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    DistributionRight distrubtionright = JsonConvert.DeserializeObject<DistributionRight>(rawData);
                    return View(distrubtionright);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/DistrubtionRight/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, DistributionRight collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/DistrubtionRight/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    DistributionRight distrubtionright = JsonConvert.DeserializeObject<DistributionRight>(rawData);
                    return View(distrubtionright);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/DistrubtionRight/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, DistributionRight collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.DISTRUBTIONRIGHT + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
