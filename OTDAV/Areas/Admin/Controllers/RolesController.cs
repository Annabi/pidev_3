﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using OTDAV.Areas.Admin.Models;
using OTDAV.Models;
using System.Collections.Generic;
using OTDAV.Core;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

namespace OTDAV.Areas.Admin.Controllers
{
    public class RolesController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        public RolesController()
        {

        }

        public RolesController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationRoleManager RoleManager { get { return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>(); } private set { _roleManager = value; } }


        //public RoleManager<IdentityRole> userRoleManager
        //{
        //    get
        //        return _roleManager ?? new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext())); ;
        //    }
        //    private set
        //    {
        //        _roleManager = value;
        //    }
        //}

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Configurator/Roles
        public async Task<ActionResult> Index()
        {
            var roles = await ((DbSet<IdentityRole>) RoleManager.Roles).ToListAsync();
            List<Role> listRoles = new List<Role>();
            roles.ForEach(r =>
            {
                Role rl = new Role();
                rl.mapToRole(r);
                listRoles.Add(rl);
            });
            return View(listRoles);
            //return View(await db.AspNetRoles.ToListAsync());
        }

        // GET: Configurator/Roles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole aspNetRoles = await RoleManager.FindByIdAsync(id);
            Role rl = new Role();
            rl.mapToRole(aspNetRoles);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(rl);
        }

        // GET: Configurator/Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Configurator/Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Role role)
        {
            if (ModelState.IsValid)
            {

                //int lastCount = db.AspNetRoles.Select(x => x.Id).Cast<int>().Max();
                //var role = db.AspNetRoles.Add(new AspNetRoles { Name = equipe.C_PA_EQ_LIBELLEEQUIPE, Id = (lastCount + 1).ToString() });
                //await db.SaveChangesAsync();
                //equipe.ROLE_ID = role.Id;
                //equipe.DisplayedForDirection = false;
                //using (IS_Team srvTeam = new S_Team(db, true, false))
                //    srvTeam.Create_Team(equipe, User.Identity.Name ?? "System");
                var r = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                r.Name = role.Name;
                RoleManager.Create(r);
                using (HttpClient client = new HttpClient())
                {
                    JavaRole jrole = new JavaRole();
                    jrole.name = role.Name;
                    client.PrepareForRequest(JavaSpring.ROLES);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.ROLES + "/save", jrole);
                }
                return RedirectToAction("Index");
            }
            return View(role);
        }

        // GET: Configurator/Roles/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole aspNetRoles = await RoleManager.FindByIdAsync(id);
            Role rl = new Role();
            rl.mapToRole(aspNetRoles);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(rl);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Role aspNetRoles)
        {
            if (ModelState.IsValid)
            {
                await RoleManager.UpdateAsync(aspNetRoles);
                using (HttpClient client = new HttpClient())
                {
                    JavaRole jrole = new JavaRole();
                    jrole.name = aspNetRoles.Name;
                    jrole.id = 1;
                    client.PrepareForRequest(JavaSpring.ROLES);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.ROLES + "/save", jrole);
                }
                //await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(aspNetRoles);
        }

        // GET: Configurator/Roles/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole aspNetRoles = await RoleManager.FindByIdAsync(id);
            Role rl = new Role();
            rl.mapToRole(aspNetRoles);
            if (aspNetRoles == null)
            {
                return HttpNotFound();
            }
            return View(rl);
        }

        // POST: Configurator/Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            IdentityRole aspNetRoles = await RoleManager.FindByIdAsync(id);
            await RoleManager.DeleteAsync(aspNetRoles);
            using (HttpClient client = new HttpClient())
            {
                client.PrepareForRequest(JavaSpring.ROLES + "/" + aspNetRoles.Name);
                HttpResponseMessage response = await client.DeleteAsync(JavaSpring.ROLES + "/delete/" + aspNetRoles.Name);
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}