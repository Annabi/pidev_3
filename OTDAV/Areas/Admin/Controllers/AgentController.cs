﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OTDAV.Areas.Admin.Models;
using OTDAV.Core;

namespace OTDAV.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class AgentController : Controller
    {


        // GET: Admin/Agent
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.AGENTS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.AGENTS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Agent> list = JsonConvert.DeserializeObject<ICollection<Agent>>(rawData);

                    foreach (Agent agent in list)
                    {
                        using (HttpClient client1 = new HttpClient()) {

                            client1.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + JavaSpring.AGENTS_OF_DEPARTMENT +"/"+agent.id);
                        HttpResponseMessage response1 = await client1.GetAsync(JavaSpring.DEPARTMENTS + "/" + JavaSpring.AGENTS_OF_DEPARTMENT+"/"+agent.id);
                        if (response1.IsSuccessStatusCode)
                            System.Diagnostics.Debug.WriteLine(response1.IsSuccessStatusCode);
                        {
                            string rawData1 = await response1.Content.ReadAsStringAsync();
                            Department dep = JsonConvert.DeserializeObject<Department>(rawData1);



                                agent.departement= dep;
                        }
                        }
                    }
                    


                    return View(list);
                }
                else
                {
                    return View(new List<Agent>());
                }
            }
        }


        // GET: Admin/Agent/Details/5
        public async Task<ActionResult> Details(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.AGENTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.AGENTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Agent agent = JsonConvert.DeserializeObject<Agent>(rawData);

                    using (HttpClient client1 = new HttpClient())
                    {

                        client1.PrepareForRequest(JavaSpring.DEPARTMENTS + "/" + JavaSpring.AGENTS_OF_DEPARTMENT + "/" +
                                                  agent.id);
                        HttpResponseMessage response1 = await client1.GetAsync(
                            JavaSpring.DEPARTMENTS + "/" + JavaSpring.AGENTS_OF_DEPARTMENT + "/" + agent.id);
                        if (response1.IsSuccessStatusCode)
                            System.Diagnostics.Debug.WriteLine(response1.IsSuccessStatusCode);
                        {
                            string rawData1 = await response1.Content.ReadAsStringAsync();
                            Department dep = JsonConvert.DeserializeObject<Department>(rawData1);



                            agent.departement = dep;
                        }
                    }

                    return View(agent);
                }
                else
                {
                    return View();
                }
            }
        }

        // GET: Admin/Agent/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Agent/Create
        [HttpPost]
        public async Task<ActionResult> Create(Agent collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.AGENTS);
                    HttpResponseMessage response = await client.PostAsJsonAsync(JavaSpring.AGENTS, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }


        public async Task<ICollection<Department>> RetriveDepartment()
        {
            List<Department> departmentList = new List<Department>();
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.DEPARTMENTS);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.DEPARTMENTS);
                if (response.IsSuccessStatusCode)
                {
                    string rawData = await response.Content.ReadAsStringAsync();
                    ICollection<Department> list = JsonConvert.DeserializeObject<ICollection<Department>>(rawData);
                    return list;
                }
            }
            return null;
        }





        // GET: Admin/Agent/Edit/5
        public async Task<ActionResult> Edit(int id)
        {


            //using (HttpClient clt = new HttpClient())
            //{
            //    //this function add headers and other things like request return format ( json )
            //    clt.PrepareForRequest(JavaSpring.DEPARTMENTS);
            //    HttpResponseMessage rsp = await clt.GetAsync(JavaSpring.DEPARTMENTS);
            //    if (rsp.IsSuccessStatusCode)
            //    {
            //        string rwData = await rsp.Content.ReadAsStringAsync();
            //        // ICollection<Department> 
            //        IEnumerable <Department> depts = JsonConvert.DeserializeObject<ICollection<Department>>(rwData);

            //        ViewBag.listDep = new SelectList(depts, "depts", "Dept");


                    using (HttpClient client = new HttpClient())
                    {
                        //this function add headers and other things like request return format ( json )
                        client.PrepareForRequest(JavaSpring.AGENTS + "/" + id);
                        HttpResponseMessage response = await client.GetAsync(JavaSpring.AGENTS + "/" + id);
                        if (response.IsSuccessStatusCode)
                        {
                            var rawData = await response.Content.ReadAsStringAsync();
                            Agent agent = JsonConvert.DeserializeObject<Agent>(rawData);


                            try
                            {
                                using (HttpClient clt = new HttpClient())
                                {
                                    ICollection<Department> result = Task.Run(RetriveDepartment).Result;
                                    List<SelectListItem> ListDepartment = result.Select(dept =>
                                    {
                                        return new SelectListItem() { Text = dept.name, Value = dept.id.ToString() };
                                    }).ToList();
                                    ViewBag.ListDepartment= ListDepartment;
                                    return View(agent);
                                }
                            }
                            catch
                            {
                                return View();
                            }

                    return View(agent);
                        }
                        else
                        {
                            return View();
                        }
                    }
               // }
                //{
                //    return View();
                //}

            //}










        }

        // POST: Admin/Agent/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Agent collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.AGENTS + "/" + id);
                    HttpResponseMessage response = await client.PutAsJsonAsync(JavaSpring.AGENTS + "/" + id, collection);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Agent/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                //this function add headers and other things like request return format ( json )
                client.PrepareForRequest(JavaSpring.AGENTS + "/" + id);
                HttpResponseMessage response = await client.GetAsync(JavaSpring.AGENTS + "/" + id);
                if (response.IsSuccessStatusCode)
                {
                    var rawData = await response.Content.ReadAsStringAsync();
                    Agent agent = JsonConvert.DeserializeObject<Agent>(rawData);
                    return View(agent);
                }
                else
                {
                    return View();
                }
            }
        }

        // POST: Admin/Agent/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(string id, Agent collection)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    //this function add headers and other things like request return format ( json )
                    client.PrepareForRequest(JavaSpring.AGENTS + "/" + id);
                    HttpResponseMessage response = await client.DeleteAsync(JavaSpring.AGENTS + "/" + id);
                    if (response.IsSuccessStatusCode)
                    {
                        var rawData = await response.Content.ReadAsStringAsync();
                        //Membership membership = JsonConvert.DeserializeObject<Membership>(rawData);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
