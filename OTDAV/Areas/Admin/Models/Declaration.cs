﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Declaration
    {
        public int id { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        //public String m_ID { get; set; }
        //public String m_Title { get; set; }
        //public String m_Description { get; set; }
        //public MemberShip m_MemberShip { get; set; }
        //public DeclarationCategory m_Category { get; set; }
    }
}