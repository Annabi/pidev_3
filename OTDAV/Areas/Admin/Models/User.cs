﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public enum LEGALFORM
    {
        SA,
        SCA,
        SNC,
        SARL,
        SUARL
    }

    public class User : Person
    {
        public bool moral { get; set; }

        public string raisonSocial { get; set; }

        public string formeJuridique { get; set; }
        public string identifiantUnique { get; set; }
    }
}