﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public enum MemberShip_Stat
    {

        Accepted,
        Rejected,
        In_progress,
        Suspended
    }

    public class Notification
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public string description { get; set; }
        public string stat { get; set; }
    }
}