﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Person
    {
        public int id { get; set; }
        [Display(Name = "CIN")]
        public string cin { get; set; }
        //public JavaRole role { get; set; }
        [Display(Name = "Subscribed")]
        public bool isSubscribed { get; set; }
        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        //public List<MemberShip> m_MemberShips { get; set; }

        public Role role { get; set; }
    }
}