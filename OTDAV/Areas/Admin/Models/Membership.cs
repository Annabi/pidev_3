﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Membership
    {
        public int id { get; set; }

        [Display(Name = "Status")]
        public string stat { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }
        public string name { get; set; }
        public List<ConstituentFile> constituentFile { get; set; }
        public List<Declaration> declarations { get; set; }
        public User user { get; set; }
        public List<DistributionRight> distributionRights { get; set; }
        public List<Work> works { get; set; }

    }
}