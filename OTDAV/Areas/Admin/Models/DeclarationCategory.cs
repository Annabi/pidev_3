﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class DeclarationCategory
    {
        public String m_ID { get; set; }
        public String m_Title { get; set; }
        public String m_Description { get; set; }
        public List<Declaration> m_Declarations { get; set; }
    }
}