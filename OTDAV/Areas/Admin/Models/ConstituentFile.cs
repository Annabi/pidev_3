﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class ConstituentFile
    {
        public int id { get; set; }
        public string path { get; set; }
        public Membership membership { get; set; }

        //public String m_ID { get; set; }
        //public String m_Path { get; set; }
        //public MemberShip m_MemberShip { get; set; }
    }
}