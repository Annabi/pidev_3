﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class DistributionRight
    {
        public int id { get; set; }
        [Display(Name = "Description")]
        public string description { get; set; }
        [Display(Name = "Starting Date")]
        public DateTime startingDate { get; set; }
        [Display(Name = "Finishing Date")]
        public DateTime finishingDate { get; set; }
        [Display(Name = "Price")]
        public double price { get; set; }
        public Membership membership { get; set; }
        public Work work { get; set; }

    }
}