﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Work
    {
        public int id { get; set; }
        [Display(Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Description")]
        public string description { get; set; }
        [Display(Name = "Distribution Rights")]
        public List<DistributionRight> distributionRights { get; set; }
        [Display(Name = "Work Category")]
        public WorkCategory workcategorie { get; set; }
        public Membership membership { get; set; }

        public Notification notification { get; set; }
    }
}