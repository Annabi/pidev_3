﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Guest : Person
    {
        [Display(Name = "IP Adress")]
        public string ipaddress { get; set; }
    }
}