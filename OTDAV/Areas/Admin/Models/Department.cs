﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models

{
    public class Department
    {
        public int id { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Description")]

        public string description { get; set; }

        public IEnumerable<Agent> Agents { get; set; }
    }
}