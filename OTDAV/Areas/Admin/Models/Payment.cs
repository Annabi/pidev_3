﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public class Payment
    {
        public String m_ID { get; set; }
        public float m_Amount { get; set; }
        public String m_Description { get; set; }
    }
}