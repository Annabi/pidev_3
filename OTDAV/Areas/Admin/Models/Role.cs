﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OTDAV.Areas.Admin.Models
{
    public enum RoleType
    {
        User,
        Guest,
        Agent,
        Administrator
    }
    public class Role : IdentityRole
    {

        //public string description { get; set; }

        //public int type { get; set; }
    }

    public class JavaRole
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}